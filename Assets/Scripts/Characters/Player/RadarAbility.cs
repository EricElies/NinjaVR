﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarAbility : MonoBehaviour
{
    public Animator animator;
    public float cooldown = 0f;
    public float cooldownTime = 10f;
    // Canvas message to player for light
    //public Canvas Canvas;
    public ParticleSystem efectoParticulas;
    public GameObject player;
    public SoundManagerScript sm;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        //Canvas.enabled = true;
        var particulas = efectoParticulas.emission;
        particulas.enabled = false;


    }

    void Update()
    {
        //Hay que ponerle cooldown
        cooldown -= Time.deltaTime;
        if (Input.GetButtonDown("Fire2") && cooldown <= 0 )
        {
            StartCoroutine("light");
            //Canvas.enabled = false;
            cooldown = cooldownTime;
            animator.SetTrigger("LightTrigger");
            
        }

        

    }
    IEnumerator light(){
            sm.PlaySound("light");
            yield return null;
        }

    void canvasActive() 
    {
        //Canvas.enabled = true;
    }

    void particleActiveEffectLight() 
    {
        efectoParticulas.transform.position = player.transform.position ;
        var particulas = efectoParticulas.emission;
        particulas.enabled = true;
        efectoParticulas.Play();
    }

}
