﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTimeSkill : MonoBehaviour
{
    //Ralentització menor
    [SerializeField]
    private float lowSlow;

    //Ralentització mitjana
    [SerializeField]
    private float mediumSlow;

    //Ralentització forta
    [SerializeField]
    private float highSlow;

    //Valor de velocitat default
    private float normalTimeScale = 1;

    private float fixedDeltaTime;

    //Bool - Estat
    [SerializeField]
    private bool stateSlow;

    //Velocitat del player
    private CharacterController chController;
    private Vector3 playerVelocity;

    //Duració habilitat
    private float duration = 3f;

    //Cooldown habilitat
    private float cooldown = 5f;

    //Particle effect feedback
    public ParticleSystem efectoParticulas;

    public AudioSource aso;

    private void Awake()
    {
        this.fixedDeltaTime = Time.fixedDeltaTime;
    }

    private void Start()
    {
        var particulas = efectoParticulas.emission;
        particulas.enabled = false;
        stateSlow = false;
        chController = GetComponent<CharacterController>();
    }

    public void SlowSkillOn()
    {
        Enemy.PlayerSpotted -= SlowSkillOn;
        //Debug.Log("ON SLOWSKILL");
        stateSlow = true;
        var particulas = efectoParticulas.emission;
        particulas.enabled = true;
        StartCoroutine(SlowDuration());
    }

    private IEnumerator SlowDuration()
    {
        
        yield return new WaitForSeconds(duration);
        SlowSkillOff();
    }

    public void SlowSkillOff()
    {
        //Debug.Log("OFF SLOWSKILL");
        stateSlow = false;
        var particulas = efectoParticulas.emission;
        particulas.enabled = false;
        StartCoroutine(SlowCooldown());
    }

    private IEnumerator SlowCooldown()
    {
        yield return new WaitForSeconds(cooldown);
        Enemy.PlayerSpotted += SlowSkillOn;
    }

    private void Update()
    {
        //Obté constantment la velocitat del player
        playerVelocity = chController.velocity;

        SlowStateCheck();
    }

    //Segons si està activat o no el Slow, el fa funcionar
    private void SlowStateCheck()
    {
        if (stateSlow)
        {
            
            SlowMode();
        }
        else if (!stateSlow)
        {
            aso.pitch=1.0f;
            //Retorna la velocitat a la normalitat
            Time.timeScale = normalTimeScale;
            Time.fixedDeltaTime = this.fixedDeltaTime * Time.timeScale;
        }
    }

    private void SlowMode()
    {
        //Ara mateix hi han 3 slows determinats per 3 franges diferents de velocitat:

        //A més de 8 de velocitat (ara mateix el màxim es 10 al correr)
        if (playerVelocity.x > 8 || playerVelocity.z > 8)
        {
            //aso.pitch=0.3f;
            SlowSpeed(lowSlow);
        }
        //Entre 4 i 8 (ara mateix 6 es el valor màxim al caminar)
        else if (playerVelocity.x > 4 || playerVelocity.z > 4)
        {
            //aso.pitch=0.3f;
            SlowSpeed(mediumSlow);
        }
        //Entre 0 i 4 (parat o començant a caminar)
        else if (playerVelocity.x >= 0 || playerVelocity.z >= 0)
        {
            //aso.pitch=0.3f;
            SlowSpeed(highSlow);
        }
    }

    //Aplica la velocitat pasada per 'slow' al temps
    private void SlowSpeed(float slow)
    {
        
        Time.timeScale = slow;
        Time.fixedDeltaTime = this.fixedDeltaTime * Time.timeScale;
    }

    private void OnEnable()
    {
        Enemy.PlayerSpotted += SlowSkillOn;
    }

    private void OnDisable()
    {
        Enemy.PlayerSpotted -= SlowSkillOn;
    }
}
