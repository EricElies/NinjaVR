﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject prefab;
    public Camera cam;
    private Transform cameraTransform;
    //Animator unnecessary for VR
    //public Animator animator;

    public SoundManagerScript sm;
    public AudioSource au;
    // Start is called before the first frame update
    void Start()
    {
        cameraTransform = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {

            shoot();
        }

    }


    public void shoot()
    {
        au.pitch = 1.0f;
        sm.PlaySound("shoot");
        Ray r = cam.ScreenPointToRay(Input.mousePosition);
        Vector3 dir = r.GetPoint(2) - r.GetPoint(0);
        Quaternion rotation = Quaternion.Euler(new Vector3(0, cameraTransform.rotation.y, cameraTransform.rotation.z));
        GameObject bullet = Instantiate(prefab, firePoint.transform.position, rotation);
        bullet.GetComponent<Rigidbody>().velocity = r.direction * 30;
        StartCoroutine(Elim(bullet));
    }
    IEnumerator Elim(GameObject bullet)
    {

        yield return new WaitForSeconds(7f);
        Destroy(bullet);
    }
}
