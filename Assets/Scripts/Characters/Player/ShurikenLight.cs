﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenLight : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip ac;
    public AudioSource auso;
    public GameObject particulas;
    void Start()
    {

        particulas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision){
        if(!collision.collider.CompareTag("Enemy")&&!collision.collider.CompareTag("Player")){
            particulas.SetActive(true);
        }
        if(collision.collider.name=="training_dummy"||collision.collider.name=="training_dummy (1)"||collision.collider.name=="training_dummy (2)"){
            particulas.SetActive(true);    
        }

        //auso.clip=ac;
        auso.PlayOneShot(ac);
        //particulas.SetActive(true);
    }
}
