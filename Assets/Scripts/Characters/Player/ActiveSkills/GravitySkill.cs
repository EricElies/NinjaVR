﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySkill : OffensiveSkills
{
    [SerializeField]
    private GameObject scanner;
    private List<GameObject> scannedObjects;
    private Transform cameraTransform;
    [SerializeField]
    private float gravPower;

    public ParticleSystem particle;
    public ParticleSystem particle1;

    private bool gravPushPull;
    //False = Push
    //True = Pull

    public SoundManagerScript sm;

    protected override void Start()
    {
        base.Start();
        var p = particle.emission;
        p.enabled = false;
        var p1 = particle1.emission;
        p1.enabled = false;
        cameraTransform = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void SkillFunctionality()
    {
        //Será mantenido el botón, not yet implemented
        //1 es empujar - Push
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            //Push

            sm.PlaySound("Gout");
            gravPushPull = false;
            Scan();

        }

        //2 es tirar - Pull
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            //Pull
            sm.PlaySound("Gout");
            gravPushPull = true;
            Scan();

        }
    }

    private void Push()
    {
        foreach (GameObject obj in scannedObjects)
        {
            var p = particle.emission;
            p.enabled = true;
            particle.Play();
            Vector3 dir = obj.transform.position - transform.position;
            dir = dir.normalized;
            obj.GetComponent<Rigidbody>().AddForce(dir * gravPower);
        }
    }

    private void Pull()
    {
        foreach (GameObject obj in scannedObjects)
        {
            var p = particle1.emission;
            p.enabled = true;
            particle1.Play();
            Vector3 dir = obj.transform.position - transform.position;
            dir = dir.normalized;
            obj.GetComponent<Rigidbody>().AddForce(dir * -gravPower);
        }
    }

    //Manda un escaneo enfrente del player, el escaneo será más amplio según el tiempo mantenido al convocarlo
    private void Scan()
    {
        Instantiate(scanner, transform.position, cameraTransform.rotation);
    }

    //Recibe la lista de objetos escaneados
    public void TransmitScanList(List<GameObject> scannedList)
    {
        scannedObjects = scannedList;
        if (!gravPushPull)
        {
            Push();
        }
        else if (gravPushPull)
        {
            Pull();
        }
    }
}
