﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skills : MonoBehaviour
{
    public int uses;
    public float durationTime;
    public float stuntTime;


    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        //Script de entrar en el modo de uso de habilidades
        //(mostrar la interfaz con las habilidades)

        //Script del recuento de usos y impedimento de uso de habilidades al llegar a 0
        UsesCheck();
    }

    protected void UsesCheck()
    {
        if (uses > 0)
        {
            //En ese Script se contiene la funcionalidad y comportamientos de las habilidades, el funcionamiento de lo usos es global
            SkillFunctionality();
        }
    }

    protected virtual void SkillFunctionality()
    {
        //Aquí se pone la funcionalidad
        //uses -= 1;
    }
}
