﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefensiveSkills : Skills
{
    //La capa invisible te volvería indetectable, 
    //pero habilidades como la bomba de humo, al ser un arrojadizo,
    //lo que hacen es impedir la visión, no volverte indetectable per se
    //
    //Es decir, este bool es prematuro, dependerá de cómo se implementen las mecánicas de sigilo
    public bool undetectable;
}
