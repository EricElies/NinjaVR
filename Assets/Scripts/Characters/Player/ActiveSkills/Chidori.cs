﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chidori : OffensiveSkills
{
    [SerializeField] private float range;
    [SerializeField] private GameObject effect;
    private CharacterController playerChC;

     public SoundManagerScript sm;
    protected override void Start()
    {
        base.Start();
        playerChC = GetComponent<CharacterController>();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void SkillFunctionality()
    {
        base.SkillFunctionality();

        if (Input.GetKeyDown(KeyCode.K))
        {
            ChidoriAbility();
        }
    }

    private void ChidoriAbility()
    {
        
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, range))
        {
            if (hit.collider.CompareTag("Enemy"))
            {
                sm.PlaySound("chidori");
                StartCoroutine(ActionChidori(hit.point, hit.collider.gameObject));
            }
        }
    }

    IEnumerator ActionChidori(Vector3 target, GameObject enemyTarget)
    {
        effect.SetActive(true);
        playerChC.enabled = false;

        yield return new WaitForSeconds(1f);
        while (Vector3.Distance(transform.position, target) > 1)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, 100);
            yield return 0;
        }

        yield return new WaitForSeconds(0.1f);
        EventHit(enemyTarget);
        effect.SetActive(false);
        playerChC.enabled = true;
    }

    private void EventHit(GameObject enemyTarget)
    {
        if (enemyTarget.GetComponent<TargetScript>())
        {
            enemyTarget.GetComponent<TargetScript>().ChidoriHit();
        }
        else if (enemyTarget.GetComponent(typeof(Enemy)))
        {
            if (enemyTarget.GetComponent<EnemySwordBlind>())
            {
                enemyTarget.GetComponent<EnemySwordBlind>().ChidoriHit();
            }
        } else if (enemyTarget.GetComponent<Boss>())
            {
               enemyTarget.GetComponent<Boss>().health-=5;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * range);
    }
}
