﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerBehaviour : MonoBehaviour
{
    [SerializeField]
    private float scanSpeed;
    private Rigidbody rb;
    private Vector3 moveDir;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        moveDir = transform.forward * scanSpeed * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        //La velocidad del escaneo y su desplazamiento
        rb.MovePosition(transform.position + moveDir);
    }

    //Cuando el escaneo se termina, esta función es llamada para destruir el objeto
    public void TimesOver()
    {
        Destroy(gameObject);
    }
}
