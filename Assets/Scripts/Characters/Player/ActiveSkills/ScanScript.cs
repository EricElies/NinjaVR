﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanScript : MonoBehaviour
{
    //Lista de los objetos escaneados
    [SerializeField]
    private List<GameObject> scannedObjects;

    //Tiempo que dura el escaneo en marcha
    [SerializeField]
    private float lifeTime;
    private GameObject player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            //Al terminar, envia la lista recogida de GameObjects al componente de la habilidad gravitatoria del Player
            player.GetComponent<GravitySkill>().TransmitScanList(scannedObjects);
            //Y entonces se destruye a si mismo, concretamente a su objeto parent
            GetComponentInParent<ScannerBehaviour>().TimesOver();   
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Si el trigger es Player, es ignorado
        if (!other.gameObject.CompareTag("Player"))
        {
            //Cada objecto que alcance el scanner que contenga un componente Rigidbody (AKA afectado por la gravedad) se añade a la lista
            if (other.gameObject.GetComponent<Rigidbody>() != null)
            {
                //Debug.Log("Objeto pillado: " + other.gameObject.name);
                scannedObjects.Add(other.gameObject);
            }
        }
    }
}
