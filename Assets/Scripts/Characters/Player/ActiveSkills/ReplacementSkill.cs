﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplacementSkill : DefensiveSkills
{
    [SerializeField] private GameObject replacementObject;
    [SerializeField] private float summonDistance;
    [SerializeField] private float summonHeight;
    private Transform teleportLocation;
    private Transform playerTransform;

    public SoundManagerScript sm;
    protected override void Start()
    {
        base.Start();
        playerTransform = transform;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void SkillFunctionality()
    {
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SummonReplacement();
        }
    }

    private void SummonReplacement()
    {
        sm.PlaySound("Sreplacement");
        Vector3 replacementLocation = transform.position + (transform.forward * summonDistance);
        replacementLocation.y += summonHeight;
        Instantiate(replacementObject, replacementLocation, transform.rotation);
    }
}
