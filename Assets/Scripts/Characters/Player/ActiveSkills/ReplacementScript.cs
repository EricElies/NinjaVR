﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReplacementScript : MonoBehaviour
{
    public delegate void ReplacementAction();
    public static event ReplacementAction ReplacementSummoned;

    [SerializeField] private string tutorialSceneName;

    [SerializeField] private float distractionDuration;
    [SerializeField] private float distractionRadius;
    private Collider[] nearColliders;

    void Start()
    {
        StartCoroutine(DistractionCountdown());
        if (SceneManager.GetActiveScene().name == tutorialSceneName)
        {
            ReplacementSummoned?.Invoke();
        }
    }

    private IEnumerator DistractionCountdown()
    {
        DistractionON();
        yield return new WaitForSeconds(distractionDuration);
        DistractionOFF();
    }

    private void DistractionON()
    {
        nearColliders = Physics.OverlapSphere(transform.position, distractionRadius);
        foreach (Collider nearCollider in nearColliders)
        {
            if (nearCollider.CompareTag("Enemy"))
            {
                if (nearCollider.GetComponent<EnemySwordBlind>())
                {
                    nearCollider.GetComponent<EnemySwordBlind>().DisableVision();
                }

                //Lo siguiente es en caso de implementar mas tipos de enemigos (ahora solo hay espadachines)

                //  else if (nearCollider.GetComponent<OtroTipoDeEnemigo>())
                // {
                //   nearCollider.GetComponent<OtroTipoDeEnemigo>().siego();
                // }
            }
        }
    }

    private void DistractionOFF()
    {
        foreach (Collider nearCollider in nearColliders)
        {
            if (nearCollider.CompareTag("Enemy"))
            {
                if (nearCollider.GetComponent<EnemySwordBlind>())
                {
                    nearCollider.GetComponent<EnemySwordBlind>().EnableVision();
                }

                //Lo siguiente es en caso de implementar mas tipos de enemigos (ahora solo hay espadachines)

                //  else if (nearCollider.GetComponent<OtroTipoDeEnemigo>())
                // {
                //   nearCollider.GetComponent<OtroTipoDeEnemigo>().siego();
                // }
            }
        }
    }
}
