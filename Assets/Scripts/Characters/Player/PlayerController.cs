﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    CharacterController characterController;
    [Header("Opciones de personaje")]
    public float walkSpeed = 6.0f;
    public float runSpeed = 10.0f;
    public float stealhthSpeed = 4.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    [Header("Opciones de camara")]
    public Camera cam;
    public float mouseHorizontal = 3.0f;
    public float mouseVertical = 2.0f;

    public int life = 10;

    public float minRotation = -65.0f;
    public float maxRotation = 60.0f;
    float h_mouse, v_mouse;


    private Vector3 move = Vector3.zero;
    private bool isCrouch = false;
    public float ColdownSense = 5f;
    private bool isDead = false;

    //Particle effect hurting
    public GameObject efectoParticulas;



    public SoundManagerScript sm;
    public AudioSource au;

    public Volume sfx;

     


    void Start()
    {
        au.volume=sfx.volume;
        // Dissable effect hurting
        efectoParticulas.SetActive(false);

        characterController = GetComponent<CharacterController>();

        Cursor.lockState = CursorLockMode.Locked;
        
    }

    void Update()
    {
        
        h_mouse = mouseHorizontal * Input.GetAxis("Mouse X");
        v_mouse += mouseVertical * Input.GetAxis("Mouse Y");

        v_mouse = Mathf.Clamp(v_mouse, minRotation, maxRotation);


        cam.transform.localEulerAngles = new Vector3(-v_mouse, 0, 0);

        transform.Rotate(0, h_mouse, 0);


        if (characterController.isGrounded)
        {
            move = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));

            if(move!=Vector3.zero&&Input.GetKey(KeyCode.LeftShift)==false){
                sm.PlaySound("walk");
            }

            if (Input.GetKey(KeyCode.LeftShift))
            {
                
                sm.PlaySound("run");
                move = transform.TransformDirection(move) * runSpeed;
                isCrouch = false;
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.C))
                {
                    if (!isCrouch)
                    {
                        isCrouch = true;
                        
                    }
                    else if (isCrouch)
                    {
                        isCrouch = false;
                        
                    }

                }
                if (isCrouch)
                {
                    move = transform.TransformDirection(move) * stealhthSpeed;
                    au.pitch=0.7f;
                }
                else
                {
                    move = transform.TransformDirection(move) * walkSpeed;
                    au.pitch=1f;
                    
                }

            }

            if (Input.GetKey(KeyCode.Space))
            {
                move.y = jumpSpeed;
            }
        }
        move.y -= gravity * Time.deltaTime;

        characterController.Move(move * Time.deltaTime);

    }

    public void ReceiveDamage()
    {
        efectoParticulas.SetActive(true);
        life -= 1;
        Invoke("DisableP", 2);
    }

    private void DisableP()
    {
        efectoParticulas.SetActive(false);
    }


    private void OnCollisionEnter(Collision collision)
    {

        Debug.Log("hay colision");
        if (collision.gameObject.CompareTag("WeaponEnemy"))
        {
            Debug.Log("Enemy te ha pegado");
            ReceiveDamage();


            if (life <= 0)
            {
                Debug.Log("Has Muerto");

            }
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("hay colision");
        if (other.gameObject.CompareTag("WeaponEnemy"))
        {
            Debug.Log("Enemy te ha pegado");
            ReceiveDamage();
        }

        if (life <= 0)
        {
            Debug.Log("Has Muerto");
            //antes el script estaba pendiente de si el canvas estaba "setActive = true" además de que detectaba que el player seguia ejecutandose y eso daba conflictos en la ejecucion
            //ahora espera a que la condicion isDead deje de ser false
            //cuando la condicion isDead es true desactiva al player y redirecciona la ejecucion de codigo a GameManager 
            Die();
        }
    }


    public void Die()
    {
        if (!isDead)
        {
            isDead = true;
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
            FindObjectOfType<GameManager>().EndGame();
        }
    }

}