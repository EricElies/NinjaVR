﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaDeadEye : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.tag);
        if (other.gameObject.CompareTag("WeaponEnemy") || other.gameObject.CompareTag("Enemy"))
        {
            GetComponentInParent<SlowTimeSkill>().SlowSkillOn();
        }
    }
}
