﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    //Variables, campos y propiedades

    /*  Estados y valor de estado   */
    [SerializeField] protected AIEnemyStates currentState;

    protected enum AIEnemyStates
    {
        Patrol, Search, Persecution, Fight, Death
    }


    /*  Parametros enemigo   */
    [SerializeField] protected float enemyHealth;
    [SerializeField] protected float distanceToStartFight;
    [SerializeField] protected float timeBetweenAttacks;
    [SerializeField] protected bool usesShuriken;
    [SerializeField] protected bool usesSword;


    /*  Componentes del enemigo   */
    [SerializeField] protected List<Transform> wayPoints;
    [SerializeField] protected Animator animator;
    [SerializeField] protected GameObject katanaOnBack, katanaOnHand;
    protected GameObject detectionCone;
    protected NavMeshAgent agent;
    protected Rigidbody rb;


    /*  Valores externos o influenciados por algo externo   */
    public delegate void DetectionAction();
    public static event DetectionAction PlayerSpotted;
    [SerializeField] protected Transform realTimePlayerLocation;
    [SerializeField] protected ParticleSystem bloodPS;
    protected Vector3 latestKnownPlayerLocation;
    protected bool canSeePlayer;
    protected bool gravityInfluence;


    /*  Control del pathfinding   */
    [SerializeField] protected bool isItOpenRoute;
    protected int targetWayPoint;
    protected bool movingForward = true;
    protected bool pathStat;
    protected bool pathStatChangeBlock;


    /*  Contadores   */
    //Tiempo para pasar de patrulla a busqueda
    [SerializeField] protected float timeToStartSearch;

    //Tiempo que tiene que pasar para resetear el tiempo de pasar de patrulla a busqueda (↑↑↑)
    [SerializeField] protected float timeToStopSearch;

    //Tiempo que busca antes de volver a patrulla
    [SerializeField] protected float timeSearching;

    //Tiempo para pasar de busqueda a persecución
    [SerializeField] protected float timeToStartPersecution;

    //Tiempo que tiene que pasar para resetear el tiempo de pasar de busqueda a persecución (↑↑↑)
    [SerializeField] protected float timeToStopPersecution;

    //Tiempo que persigue antes de volver a busqueda
    [SerializeField] protected float timePersecuting;

    protected float counterTimeToStartSearch;
    protected float counterTimeToStopSearch;
    protected float counterTimeSearching;
    protected float counterTimeToStartPersecution;
    protected float counterTimeToStopPersecution;
    protected float counterTimePersecuting;


    //Sound
    public SoundManagerScript sm;

    

    /*  Getters y setters   */
    public bool seeingPlayer
    {
        get => canSeePlayer;
        set
        {
            canSeePlayer = value;
            if (seeingPlayer)
            {
                PlayerSpotted?.Invoke();
            }
        }
    }

    protected float health
    {
        get => enemyHealth;
        set
        {
            enemyHealth = value;
            if (currentState != AIEnemyStates.Death)
            {
                bloodPS.Play(true);
            }

            ChangeState(AIEnemyStates.Fight);

            if (health <= 0)
            {
                currentState = AIEnemyStates.Death;
            }
        }
    }

    protected bool pathStatus
    {
        get => pathStat;
        set
        {
            pathStat = value;
            if (pathStatus)
            {
                if (pathStatChangeBlock)
                {
                    rb.isKinematic = agent.enabled = true;
                    pathStatChangeBlock = false;
                }
            }
            else if (!pathStatus)
            {
                if (!pathStatChangeBlock)
                {
                    rb.isKinematic = agent.enabled = false;
                    pathStatChangeBlock = true;
                }
            }
        }
    }

    protected bool gravInfluence
    {
        get => gravityInfluence;
        set
        {
            gravityInfluence = value;
            if (gravInfluence && currentState != AIEnemyStates.Death)
            {
                StartCoroutine(GravityInfluenceDelaysPathFinding());
            }
        }
    }



    //Funciones base
    protected virtual void Start()
    {
        detectionCone = transform.Find("Detection Cone").gameObject;
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();

        //Comprueba que no estén los dos tipos seleccionados para evitar que pete
        if (usesSword == true & usesShuriken == true)
        {
            usesShuriken = false;
        }

        //Desactiva la katana en mano
        katanaOnHand.SetActive(false);


        ChangeState(AIEnemyStates.Patrol);
    }

    protected virtual void Update()
    {
        ExecuteState(currentState);
    }


    protected void ExecuteState(AIEnemyStates state)
    {
        switch (state)
        {
            case AIEnemyStates.Patrol:
                ResumePathFinding();
                Patrol();
                break;
            case AIEnemyStates.Search:
                Search();
                break;
            case AIEnemyStates.Persecution:
                ResumePathFinding();
                Persecution();
                break;
            case AIEnemyStates.Fight:
                PausePathFinding();
                Fight();
                break;
            case AIEnemyStates.Death:
                PausePathFinding();

                Death();
                break;
        }
    }

    protected void ChangeState(AIEnemyStates state)
    {
        currentState = state;
        if (currentState == AIEnemyStates.Search)
        {
            SetLatestPlayerLocation();
        }


        counterTimeToStartSearch = counterTimeToStopSearch = counterTimeSearching = 0;
        counterTimeToStartPersecution = counterTimeToStopPersecution = counterTimePersecuting = 0;
    }

    //Funciones de estado
    protected virtual void Patrol()
    {
        animator.SetBool("IsWalking", true);

        PatrolDefaultBehaviour();

        if (seeingPlayer)
        {
            CounterMethod(ref counterTimeToStartSearch, timeToStartSearch, AIEnemyStates.Search);
        }

        if (!seeingPlayer & counterTimeToStartSearch > 0)
        {
            ResetCounterMethod(ref counterTimeToStopSearch, timeToStopSearch, ref counterTimeToStartSearch);
        }
    }

    protected virtual void Search()
    {
        CounterMethod(ref counterTimeSearching, timeSearching, AIEnemyStates.Patrol);

        ReachLatestPlayerLocation();

        if (seeingPlayer)
        {
            CounterMethod(ref counterTimeToStartPersecution, timeToStartPersecution, AIEnemyStates.Persecution);
        }

        if (!seeingPlayer & counterTimeToStartPersecution > 0)
        {
            ResetCounterMethod(ref counterTimeToStopPersecution, timeToStopPersecution, ref counterTimeToStartPersecution);
        }
    }

    protected virtual void Persecution()
    {
        ApproachPlayer();
        animator.SetBool("IsWalking", false);

        if (seeingPlayer)
        {
            counterTimePersecuting = 0;
        }

        if (!seeingPlayer)
        {
            CounterMethod(ref counterTimePersecuting, timePersecuting, AIEnemyStates.Search);
        }
    }

    protected virtual void Fight()
    {
        PausePathFinding();

        FightDefaultBehaviour();

        if (CheckDistance(realTimePlayerLocation) > distanceToStartFight)
        {
            if (!gravInfluence)
            {
                ExitSwordFightMode();
                ChangeState(AIEnemyStates.Persecution);
            }
        }
    }

    protected virtual void Death()
    {
        StartCoroutine("deathsound");
        PausePathFinding();
        Debug.Log(gameObject.name + " ha muerto");
        animator.SetTrigger("IsDead");
        rb.IsSleeping();
        gameObject.tag = "EnemyDead";
        //sm.enabled=false;

    }

    IEnumerator deathsound(){
        if(sm.enabled==true){
        sm.PlaySound("deathenemy");
        sm.enabled=false;
        yield return null;
        }   
        
    }



    //Control del PathFinding
    protected virtual void PausePathFinding()
    {
        pathStatus = false;
    }

    protected virtual void ResumePathFinding()
    {
        pathStatus = true;
    }



    //Comportamiento de la patrulla
    protected virtual void PatrolDefaultBehaviour()
    {
        if (wayPoints.Count <= 0) return;
        if (wayPoints[targetWayPoint] == null) return;
        if (wayPoints.Count == 1)
        {
            NoRoute();
        }
        if (isItOpenRoute)
        {
            PatrolOpenRoute();
        }
        else if (!isItOpenRoute)
        {
            PatrolCloseRoute();
        }
    }

    protected virtual void NoRoute()
    {
        animator.SetBool("IsWalking", false);
    }

    protected virtual void PatrolCloseRoute()
    {
        if (wayPoints.Count < 0) return;
        if (wayPoints[targetWayPoint] == null) return;

        agent.SetDestination(wayPoints[targetWayPoint].position);

        if (CheckDistance(wayPoints[targetWayPoint]) <= 1.5f)
        {
            if (wayPoints.Count - 1 > targetWayPoint)
            {
                targetWayPoint++;
            }
            else if (wayPoints.Count - 1 <= targetWayPoint)
            {
                targetWayPoint = 0;
            }
        }
    }

    protected virtual void PatrolOpenRoute()
    {
        if (wayPoints.Count <= 0) return;
        if (wayPoints[targetWayPoint] == null) return;

        agent.SetDestination(wayPoints[targetWayPoint].position);

        if (CheckDistance(wayPoints[targetWayPoint]) <= 1.5f)
        {
            if (movingForward)
            {
                targetWayPoint++;
                if (targetWayPoint == wayPoints.Count)
                {
                    movingForward = false;

                    targetWayPoint--;
                }
            }
            else
            {
                targetWayPoint--;
                if (targetWayPoint == 0)
                {
                    movingForward = true;
                }
            }
        }
    }



    //Funciones para medir distancias
    protected virtual float CheckDistance(Transform target)
    {
        return Vector3.Distance(target.position, transform.position);
    }

    protected virtual float CheckDistance(Vector3 target)
    {
        return Vector3.Distance(target, transform.position);
    }



    //Funciones relativas a la ubicación del jugador
    protected virtual void SetLatestPlayerLocation()
    {
        latestKnownPlayerLocation = realTimePlayerLocation.position;
    }

    protected virtual void ReachLatestPlayerLocation()
    {
        agent.SetDestination(latestKnownPlayerLocation);

        if (CheckDistance(latestKnownPlayerLocation) <= 1.5f)
        {
            PausePathFinding();
            NoRoute();
        }
    }

    protected virtual void ApproachPlayer()
    {
        agent.SetDestination(realTimePlayerLocation.position);

        if (CheckDistance(realTimePlayerLocation) <= distanceToStartFight)
        {
            ChangeState(AIEnemyStates.Fight);
        }
    }



    //Funciones pelea
    protected virtual void FightDefaultBehaviour()
    {
        if (usesShuriken)
        {
            FightWithShurikens();
        }
        else if (usesSword)
        {
            SwordFightMode();
            FightWithSword();
        }
    }

    protected virtual void FightWithSword()
    {
        if (animator.GetBool("SwordAvaliable"))
        {
            if (timeBetweenAttacks <= 0)
            {
                int typeAttack = Random.Range(0, 3);
                Debug.Log("AttackType: " + typeAttack);
                switch (typeAttack)
                {
                    //case 0: animator.SetInteger("TypeAttackSword", 1); timeBetweenAttacks = 3f; break;
                    case 0: animator.SetInteger("TypeAttackSword", 2); timeBetweenAttacks = 3f; break;
                    case 1: animator.SetInteger("TypeAttackSword", 3); timeBetweenAttacks = 3f; break;
                    case 2: animator.SetInteger("TypeAttackSword", 4); timeBetweenAttacks = 3f; break;
                    default: animator.SetInteger("TypeAttackSword", 0); timeBetweenAttacks = 3f; break;
                }
            }
            else
            {
                timeBetweenAttacks -= Time.deltaTime;
            }
        }
    }

    protected virtual void FightWithShurikens()
    {

    }

    protected virtual void ExitSwordFightMode()
    {
        animator.SetBool("SwordAvaliable", false);
    }

    protected virtual void SwordFightMode()
    {
        animator.SetTrigger("TakeSword");
        animator.SetBool("SwordAvaliable", true);
    }

    protected virtual void TakeKatana()
    {
        if (animator.GetBool("SwordAvaliable"))
        {
            katanaOnBack.SetActive(false);
            katanaOnHand.SetActive(true);
        }
        else
        {
            katanaOnBack.SetActive(true);
            katanaOnHand.SetActive(false);
        }
    }



    //Funciones contador
    protected virtual void CounterMethod(ref float counter, float targetTime, AIEnemyStates state)
    {
        counter += Time.deltaTime;
        if (counter >= targetTime)
        {
            ChangeState(state);
        }
    }

    protected virtual void ResetCounterMethod(ref float counter, float targetTime, ref float resetedCounter)
    {
        counter += Time.deltaTime;
        if (counter >= targetTime)
        {
            resetedCounter = counter = 0;
        }
    }



    //Control del tiempo durante el que el enemigo se ve influenciado por físicas
    protected virtual IEnumerator GravityInfluenceDelaysPathFinding()
    {
        yield return new WaitForSeconds(3);
        gravInfluence = false;
    }



    //Detección del 'Scanner', un GameObject usado con la habilidad del jugador 'GravitySkill'
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Scanner") && currentState != AIEnemyStates.Death)
        {
            ChangeState(AIEnemyStates.Fight);
            gravInfluence = true;
        }
    }



    //Detección habilidad del jugador 'Chidori'
    public void ChidoriHit()
    {
        health = 0;
    }



    //Detección ataques del jugador
    protected virtual void OnCollisionEnter(Collision collision)
    {
       
        
        if (collision.gameObject.CompareTag("WeaponPlayer"))
        {
           if (currentState != AIEnemyStates.Death)
            {
                MeshRenderer go=collision.gameObject.GetComponent<MeshRenderer>();
                BoxCollider bc=go.GetComponent<BoxCollider>();
                GameObject gmash=go.gameObject.GetComponent<GameObject>();
                Destroy(gmash);
                bc.enabled=false;
                sm.PlaySound("shurikenhit");
                health -= 1;
               
            }
            else
            {

            }

           

            //GameObject go=collision.gameObject.GetComponent<GameObject>();
            //go.GetComponent<Rigidbody>().constraints=RigidbodyConstraints.FreezePositionX;



            

        }
        if (collision.gameObject.CompareTag("swordplayer"))
        {
            if (currentState != AIEnemyStates.Death)
            {
                new WaitForSeconds(2.0f);
                sm.PlaySound("hit");

                health -= 1;
            }
            else
            {

            }


        }
    }



    //Métodes públics per habilitar i deshabilitar la visibilitat de l'enemic
    public virtual void DisableVision()
    {
        detectionCone.SetActive(false);
        seeingPlayer = false;
        ChangeState(AIEnemyStates.Search);
    }

    public virtual void EnableVision()
    {
        detectionCone.SetActive(true);
    }
}