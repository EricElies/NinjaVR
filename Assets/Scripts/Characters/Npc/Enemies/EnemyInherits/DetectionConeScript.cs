﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionConeScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") & transform.parent.CompareTag("Enemy"))
        {
            GetComponentInParent<Enemy>().seeingPlayer = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GetComponentInParent<Enemy>().seeingPlayer = false;
        }
    }
}
