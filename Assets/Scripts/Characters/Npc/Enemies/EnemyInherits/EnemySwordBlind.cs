﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySwordBlind : Enemy
{
    
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void Death()
    {
        base.Death();
        var emission = GetComponent<ParticleSystem>().emission;
        emission.enabled = false;
        
    }
    
}
