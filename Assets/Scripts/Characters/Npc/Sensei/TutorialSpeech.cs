﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialSpeech : MonoBehaviour
{
    [SerializeField] private GameObject tutorialManager;
    [SerializeField] private string[] dialog;
    [SerializeField] private float secondsBetweenEachPhrase;
    private TMPro.TextMeshPro tmp;
    private int dialogNum;

    void Start()
    {
        tutorialManager.SetActive(false);
        tmp = GetComponent<TMPro.TextMeshPro>();
        StartCoroutine(SpeechDelay());
    }

    void Update()
    {
        tmp.text = dialog[dialogNum];
    }

    private IEnumerator SpeechDelay()
    {
        if (dialog.Length > dialogNum)
        {
            yield return new WaitForSeconds(secondsBetweenEachPhrase);
            dialogNum++;
            StartCoroutine(SpeechDelay());
        }
        else
        {
            tutorialManager.SetActive(true);
            this.enabled = false;
        }
    }
}
