﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeText : MonoBehaviour
{
    public Animator animator;

    void Start()
    {
        
    }

    public void FadeOutText() 
    {
        animator.Play("FadeOutText");
    }

    public void FadeInText()
    {
        animator.Play("FadeInText");
    }

}
