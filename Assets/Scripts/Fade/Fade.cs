﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    public Animator animator;
    public GameObject canvasPause;

    void Start()
    {
        FadeIn();
    }

    public void FadeOut() 
    {
        animator.Play("FadeOut");

    }

    public void FadeIn()
    {
        animator.Play("FadeIn");
        //StartCoroutine(FadeIn(animator.GetCurrentAnimatorStateInfo(0).length, gameObject));
        
    }

    public void FadeInBackground()
    {
        animator.Play("FadeInBackground");

    }
    public void FadeOutBackground()
    {
        animator.Play("FadeOutBackground");
    }


    // cuando la pantalla deja de estar negra

    public IEnumerator FadeIn(float time, GameObject gaOb)
    {
        yield return new WaitForSeconds(time);
        gaOb.SetActive(false);
        canvasPause.SetActive(true);
    }
}
