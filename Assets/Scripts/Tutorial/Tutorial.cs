﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TutorialType { Mechanic, Skill };
public enum TutorialMechanic { None, ShurikenThrowMechanic, DarknessMechanic, TimeDilationMechanic, StealthMechanic };
public enum TestedSkill { None, Replacement, Cloak, Smoke, Stun, Chidori, WeaponSummon, Gravity, Snake };

public class Tutorial : MonoBehaviour
{
    public string tutorialName;
    public string tutorialText;
    public int repetitions;
    public TutorialType tutorialType;
    public TutorialMechanic tutorialMechanic;
    public TestedSkill testedSkill;

    
    protected int Repetitions
    {
        get { return repetitions; }
        set
        {
            repetitions = value;
            TutorialAdvancement?.Invoke();
        }
    }

    public delegate void TutorialStatus();
    public static event TutorialStatus TutorialAdvancement;
    public static event TutorialStatus TutorialFinished;

    protected virtual void RepetitionDone()
    {
        Repetitions -= 1;
        if (Repetitions <= 0)
        {

            TutorialFinished?.Invoke();
        }
    }
}
