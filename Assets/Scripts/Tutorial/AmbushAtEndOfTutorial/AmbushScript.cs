﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbushScript : MonoBehaviour
{
    private Animator[] animators;

    void Start()
    {
        animators = GetComponentsInChildren<Animator>();
    }

    private void AmbushIsReady()
    {
        foreach (Animator animator in animators)
        {
            animator.SetTrigger("AttackSensei");
        }
    }

    private void OnEnable()
    {
        AmbushReady.AmbushIsReady += AmbushIsReady;
    }

    private void OnDisable()
    {
        AmbushReady.AmbushIsReady -= AmbushIsReady;
    }
}
