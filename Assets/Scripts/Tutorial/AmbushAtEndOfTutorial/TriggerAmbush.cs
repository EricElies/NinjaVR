﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAmbush : MonoBehaviour
{
    public delegate void AmbushAction();
    public static event AmbushAction AmbushTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            AmbushTrigger?.Invoke();
        }
    }
}
