﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_Chidori : Tutorial
{
    
    private void OnEnable()
    {
        
        TargetScript.TargetChidoriHit += RepetitionDone;
    }

    private void OnDisable()
    {
        TargetScript.TargetChidoriHit -= RepetitionDone;
    }

    protected override void RepetitionDone()
    {
        base.RepetitionDone();
    }
}
