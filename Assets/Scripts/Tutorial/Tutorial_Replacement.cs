﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_Replacement : Tutorial
{
    private void OnEnable()
    {
        ReplacementScript.ReplacementSummoned += RepetitionDone;
    }

    private void OnDisable()
    {
        ReplacementScript.ReplacementSummoned -= RepetitionDone;
    }

    protected override void RepetitionDone()
    {
        base.RepetitionDone();
    }
}
