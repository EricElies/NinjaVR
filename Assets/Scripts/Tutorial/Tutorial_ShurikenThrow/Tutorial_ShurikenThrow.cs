﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_ShurikenThrow : Tutorial
{
    private void OnEnable()
    {
        TargetScript.TargetHit += RepetitionDone;
    }

    private void OnDisable()
    {
        TargetScript.TargetHit -= RepetitionDone;
    }

    protected override void RepetitionDone()
    {
        base.RepetitionDone();
    }
}
