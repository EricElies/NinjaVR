﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour
{
    public delegate void TargetAction();
    public static event TargetAction TargetHit;
    private bool alreadyHit = false;
    public GameObject light;
    public GameObject chidorilight;
    
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("WeaponPlayer"))
        {
            if (!alreadyHit)
            {
                TargetHit?.Invoke();
                light.SetActive(false);
                alreadyHit = true;
            }else{
                //light.SetActive(false);
            }
        }
    }

    public static event TargetAction TargetChidoriHit;
    
    private bool chidoriHit = false;

    private bool chidoriHitted
    {
        get { return chidoriHit; }
        set
        {
            chidoriHit = value;
            if (chidoriHitted)
            {
                chidorilight.SetActive(false);
                TargetChidoriHit?.Invoke();
            }
        }
    }

    public void ChidoriHit()
    {
        if (!chidoriHitted)
        {
            
            chidoriHitted = true;
        }
    }
}
