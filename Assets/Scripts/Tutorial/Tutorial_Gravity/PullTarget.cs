﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullTarget : MonoBehaviour
{
    public delegate void PullAction();
    public static event PullAction PullDone;
    [SerializeField]
    private int amountOfPulls;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (amountOfPulls > 0)
            {
                PullDone?.Invoke();
                amountOfPulls -= 1;
            }
        }
    }
}
