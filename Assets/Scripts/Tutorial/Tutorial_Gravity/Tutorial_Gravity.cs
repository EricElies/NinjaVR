﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_Gravity : Tutorial
{
    private void OnEnable()
    {
        PullTarget.PullDone += RepetitionDone;
        PushTarget.PushDone += RepetitionDone;
    }

    private void OnDisable()
    {
        PullTarget.PullDone -= RepetitionDone;
        PushTarget.PushDone -= RepetitionDone;
    }


    protected override void RepetitionDone()
    {
        base.RepetitionDone();
    }
}
