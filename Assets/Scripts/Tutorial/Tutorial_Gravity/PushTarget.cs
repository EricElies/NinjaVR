﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushTarget : MonoBehaviour
{
    public delegate void PushAction();
    public static event PushAction PushDone;
    [SerializeField]
    private int amountOfPushes;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (amountOfPushes > 0)
            {
                PushDone?.Invoke();
                amountOfPushes -= 1;
            }
        }
    }
}
