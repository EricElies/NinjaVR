﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    
    //https://www.youtube.com/watch?v=FR7zDjS0mO8

    public void SetAllVolume(float volume)
    {
        audioMixer.SetFloat("overallVolume", volume);
    }

    public void SetSEVolume(float volume)
    {
        audioMixer.SetFloat("soundEffects", volume);
    }

    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("music", volume);
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
}
