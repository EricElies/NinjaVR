﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool isGamePaused = false;

    //playerScript Managers
    public PlayerController movement;
    public SlowTimeSkill slow;
    public ReplacementSkill replacement;
    public Chidori chidori;
    public Shooting shooting;
    public SworsAttack sword;
    public RadarAbility radar;
    public PlayerOnAnimation onAnimation;

    [SerializeField] GameObject pauseMenu;

    //canvas texts
    public GameObject canvasAdvices;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isGamePaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    void ResumeGame()
    {
        pauseMenu.SetActive(false);
        canvasAdvices.SetActive(true);
        Time.timeScale = 1f;
        isGamePaused = false;
        Cursor.lockState = CursorLockMode.Locked;

        //when is true, all abilities work correctly
        movement.enabled = true;
        slow.enabled = true;
        replacement.enabled = true;
        chidori.enabled = true;
        shooting.enabled = true;
        sword.enabled = true;
        radar.enabled = true;
        onAnimation.enabled = true;

    }

    void PauseGame()
    {
        pauseMenu.SetActive(true);
        canvasAdvices.SetActive(false);
        Time.timeScale = 0f;
        isGamePaused = true;
        Cursor.lockState = CursorLockMode.None;

        //when is false, all abilities and movement are deactivated and player can't move
        onAnimation.enabled = false;
        movement.enabled = false;
        slow.enabled = false;
        replacement.enabled = false;
        chidori.enabled = false;
        shooting.enabled = false;
        sword.enabled = false;
        radar.enabled = false;

    }
}
