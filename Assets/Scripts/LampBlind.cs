﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampBlind : MonoBehaviour
{
    public GameObject gameObject;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            gameObject.SetActive(true);
        }
    }
}
