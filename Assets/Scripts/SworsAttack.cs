﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SworsAttack : MonoBehaviour
{

    public Animator anim;
    public GameObject swordOnHand, swordNotHand;
    private bool swordMode;
    private Shooting shooting;
    
    public SoundManagerScript sm;

    

    // Start is called before the first frame update
    void Start()
    {
        
        swordMode = false;
        
        shooting= GetComponent<Shooting>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            
            if (!swordMode)
            {
                
               
                sm.PlaySound("out");
                swordNotHand.SetActive(false);
                swordOnHand.SetActive(true);
                swordMode = true;
            }
            else
            {
               
                sm.PlaySound("in");
                swordNotHand.SetActive(true);
                swordOnHand.SetActive(false);
                swordMode = false;
            }
        }
        
        
        if (swordMode) { 
            
            shooting.enabled = false;
            if (Input.GetButtonDown("Fire1"))
            {
               
                sm.PlaySound("attack");
                anim.SetInteger("TypeAttack", Random.Range(0, 4));
            }
            else
            {
                
                anim.SetInteger("TypeAttack", 0);
                shooting.enabled = true;
            }
        }

    }
}
