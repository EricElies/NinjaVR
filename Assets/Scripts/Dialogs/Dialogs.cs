﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Dialogs : MonoBehaviour
{
    public GameObject TextBackground;
    public GameObject[] Dialogos;
    public bool dialogEnded = false;
    public Fade fade;
    public FadeText[] fadeText;

    
    // Start is called before the first frame update
    void Start()
    {

        if (!dialogEnded)
        {
            StartCoroutine(dialogHistory());
        }
        
        
    }

    
    public IEnumerator dialogHistory()
    {
        {
            fade.FadeIn();
            yield return new WaitForSeconds(4f);
            TextBackground.SetActive(true);
            fade.FadeInBackground();
            yield return new WaitForSeconds(0.5f);

            Dialogos[0].SetActive(true);
            fadeText[0].FadeInText();
            yield return new WaitForSeconds(4f);
            fadeText[0].FadeOutText();
            yield return new WaitForSeconds(1f);
            Dialogos[0].SetActive(false);
            
            yield return new WaitForSeconds(1f); 
            
            Dialogos[1].SetActive(true);
            fadeText[1].FadeInText();
            yield return new WaitForSeconds(4f);
            fadeText[1].FadeOutText();
            yield return new WaitForSeconds(1f);
            Dialogos[1].SetActive(false);
            
            yield return new WaitForSeconds(1f);
            
            Dialogos[2].SetActive(true);
            fadeText[2].FadeInText();
            yield return new WaitForSeconds(4f);
            fadeText[2].FadeOutText();
            yield return new WaitForSeconds(1f);
            Dialogos[2].SetActive(false);
            
            yield return new WaitForSeconds(1f);
            
            Dialogos[3].SetActive(true);
            fadeText[3].FadeInText();
            yield return new WaitForSeconds(4f);
            fadeText[3].FadeOutText();
            yield return new WaitForSeconds(1f);
            Dialogos[3].SetActive(false);
            
            yield return new WaitForSeconds(1f);
            
            Dialogos[4].SetActive(true);
            fadeText[4].FadeInText();
            yield return new WaitForSeconds(4f);
            fadeText[4].FadeOutText();
            yield return new WaitForSeconds(1f);
            Dialogos[4].SetActive(false);
            
            yield return new WaitForSeconds(1f);
            
            Dialogos[5].SetActive(true);
            fadeText[5].FadeInText();
            yield return new WaitForSeconds(4f);
            fadeText[5].FadeOutText();
            yield return new WaitForSeconds(1f);
            Dialogos[5].SetActive(false);
            
            yield return new WaitForSeconds(1f);

            fade.FadeOutBackground();
            yield return new WaitForSeconds(0.5f);

            fade.FadeOut();
            yield return new WaitForSeconds(2f);
            dialogEnded = true;

    }
}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space)){
            dialogEnded = true;
        }

        if (dialogEnded) SceneManager.LoadScene("Level 1 No Blind");
        
    }
    
}
