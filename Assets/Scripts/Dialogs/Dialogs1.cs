﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Dialogs1 : MonoBehaviour
{
    public GameObject TextBackground;
    public GameObject[] Dialogos;
    public bool dialogEnded = false;
    public Fade fade;
    public FadeText[] fadeText;

    
    // Start is called before the first frame update
    void Start()
    {

        if (!dialogEnded)
        {
            StartCoroutine(dialogHistory());
        }
        
        
    }

    
    public IEnumerator dialogHistory()
    {
        {
            fade.FadeIn();
            yield return new WaitForSeconds(4f);
            TextBackground.SetActive(true);
            fade.FadeInBackground();
            yield return new WaitForSeconds(0.5f);

            Dialogos[0].SetActive(true);
            fadeText[0].FadeInText();
            yield return new WaitForSeconds(4f);
            fadeText[0].FadeOutText();
            yield return new WaitForSeconds(1f);
            Dialogos[0].SetActive(false);
            
            yield return new WaitForSeconds(1f); 
            
            Dialogos[1].SetActive(true);
            fadeText[1].FadeInText();
            yield return new WaitForSeconds(4f);
            fadeText[1].FadeOutText();
            yield return new WaitForSeconds(1f);
            Dialogos[1].SetActive(false);
            
            yield return new WaitForSeconds(1f);
            
            
            yield return new WaitForSeconds(1f);

            fade.FadeOutBackground();
            yield return new WaitForSeconds(0.5f);

            fade.FadeOut();
            yield return new WaitForSeconds(2f);
            dialogEnded = true;

    }
}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space)){
            dialogEnded = true;
        }

        if (dialogEnded) SceneManager.LoadScene("Level 1");
        
    }
    
}
