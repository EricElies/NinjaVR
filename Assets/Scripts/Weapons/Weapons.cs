﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Weapon")]
public class Weapons : ScriptableObject
{
    public string nameWeapon;
    public float speedBullet;
    public GameObject prefab;
    public int munition;
    public float dmg;
}


    



