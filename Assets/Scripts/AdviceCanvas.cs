﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdviceCanvas : MonoBehaviour
{
    public GameObject text;
    public GameObject collider;
    

    // Start is called before the first frame update
    void Start()
    {
        text.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (collider.name.Equals(text.name))
            {
                text.SetActive(true);
            }
            else
            {
                text.SetActive(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (collider.name.Equals(text.name))
                text.SetActive(false);
        }
        
    }
}
