﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] private GameObject[] tutorials;
    private int currentTutorial;
    [SerializeField] private GameObject bubbleText;
    [SerializeField] private Animator animatorAmbush;
    [SerializeField] private CharacterController playerCharController;
    private int doneRepetitions;
    private int totalRepetitions;
    private string showedText;
    private bool playerInAmbushArea;

    [SerializeField] private SoundManagerScript sm;
    private bool playerAmbush
    {
        get { return playerInAmbushArea; }
        set
        {
            playerInAmbushArea = value;
            if (playerInAmbushArea)
            {
                animatorAmbush.SetTrigger("StartAmbush");
            }
        }
    }

    private int CurrentTutorial
    {
        get { return currentTutorial; }
        set
        {
            currentTutorial = value;

            //Al cambiar el valor de CurrentTutorial, carga ese tutorial
            LoadNextTutorial();
        }
    }

    void Start()
    {
        //Empieza por el primero, es decir, el primero del array
        currentTutorial = 0;
        doneRepetitions = 0;
        foreach (GameObject tutorial in tutorials)
        {
            if (tutorial.GetComponent<Tutorial_ShurikenThrow>() == null)
            {
                tutorial.SetActive(false);
                TextUpdate();
            }
            else
            {
                FindAndResetRepetitions();
            }
        }
    }

    private void FindAndResetRepetitions()
    {
        doneRepetitions = 0;
        switch (tutorials[CurrentTutorial].GetComponent(typeof(Tutorial)).name)
        {
            case "Tutorial_ShurikenThrow":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_ShurikenThrow>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_ShurikenThrow>().tutorialText;
                break;
            case "Tutorial_TimeDilation":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_TimeDilation>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_TimeDilation>().tutorialText;
                break;
            case "Tutorial_Stealth":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_Stealth>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_Stealth>().tutorialText;
                break;
            case "Tutorial_Darkness":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_Darkness>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_Darkness>().tutorialText;
                TutorialIsOver();
                break;
            case "Tutorial_Replacement":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_Replacement>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_Replacement>().tutorialText;
                break;
            case "Tutorial_Cloak":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_Cloak>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_Cloak>().tutorialText;
                break;
            case "TutorialM_ShurikenThrow":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_Smoke>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_Smoke>().tutorialText;
                break;
            case "Tutorial_Smoke":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_Stun>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_Stun>().tutorialText;
                break;
            case "Tutorial_Chidori":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_Chidori>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_Chidori>().tutorialText;
                break;
            case "Tutorial_WeaponSummon":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_WeaponSummon>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_WeaponSummon>().tutorialText;
                break;
            case "Tutorial_Gravity":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_Gravity>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_Gravity>().tutorialText;

                break;
            case "Tutorial_SnakeBite":
                totalRepetitions = tutorials[CurrentTutorial].GetComponent<Tutorial_SnakeBite>().repetitions;
                showedText = tutorials[CurrentTutorial].GetComponent<Tutorial_SnakeBite>().tutorialText;
                break;
        }
        TextUpdate();
    }

    //Mostrar en bubble lo pedido en el tutorial (extraerlo del GameObject de tutorial mismo)
    private void TutorialProgress()
    {
        doneRepetitions += 1;
        TextUpdate();
    }

    private void TextUpdate()
    {
        bubbleText.GetComponent<TMPro.TextMeshPro>().text = showedText + "\n" + doneRepetitions + "/" + totalRepetitions;
    }

    private void ThisTutorialIsOver()
    {
        sm.PlaySound("tuto");
        //Pasa al siguiente tutorial
        CurrentTutorial += 1;
    }

    //Desactiva el tutorial previo y activa el actual
    private void LoadNextTutorial()
    {
        tutorials[CurrentTutorial - 1].SetActive(false);
        tutorials[CurrentTutorial].SetActive(true);
        FindAndResetRepetitions();

        //Apaño temporal
        if (tutorials[CurrentTutorial].name == "Tutorial_Darkness")
        {
            TutorialIsOver();
        }
    }

    private void TutorialIsOver()
    {
        bubbleText.GetComponent<TMPro.TextMeshPro>().text = "Well done, I think that's enough training. COME HERE!!, I have to tell you something.";
        TriggerAmbush.AmbushTrigger += PlayerIsInAmbushArea;
    }

    private void PlayerIsInAmbushArea()
    {
        playerAmbush = true;
        playerCharController.enabled = false;
    }

    private void OnEnable()
    {
        Tutorial.TutorialFinished += ThisTutorialIsOver;
        Tutorial.TutorialAdvancement += TutorialProgress;
    }

    private void OnDisable()
    {
        Tutorial.TutorialFinished -= ThisTutorialIsOver;
        Tutorial.TutorialAdvancement -= TutorialProgress;
        TriggerAmbush.AmbushTrigger -= PlayerIsInAmbushArea;
    }
}
