﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{   //Script que controla el estado del player, cuando sucede un evento que necesita el GameManager este es ejecutado
    public PlayerController playerController;
    //GameObjects que controla la aparicion de el panel del player cuando muere
    public GameObject CanvasDead;


    bool gameHasEnded = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    //Cuando el player pasa a tener vida <=0 EndGame se activa
    public void EndGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("GAME OVER");
            CanvasDead.SetActive(true);
            StartCoroutine(dead());
        }
    }

    IEnumerator dead()
    {
        yield return new WaitForSecondsRealtime(4f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
