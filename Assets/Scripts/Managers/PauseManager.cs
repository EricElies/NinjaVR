﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    public static bool isGamePaused = false;

    //playerScript Managers
    public PlayerController movement;
    public SlowTimeSkill slow;
    public ReplacementSkill replacement;
    public Chidori chidori;
    public Shooting shooting;
    public SworsAttack sword;
    public RadarAbility radar;
    public PlayerOnAnimation onAnimation;

    //canvas texts
    public GameObject canvasAdvices;

    public GameObject pauseMenu;

    public GameObject confirm;


    public void Resume()
    {
        pauseMenu.SetActive(false);
        canvasAdvices.SetActive(true);
        Time.timeScale = 1f;
        isGamePaused = false;
        Cursor.lockState = CursorLockMode.Locked;

        //when is true, all abilities work correctly
        movement.enabled = true;
        slow.enabled = true;
        replacement.enabled = true;
        chidori.enabled = true;
        shooting.enabled = true;
        sword.enabled = true;
        radar.enabled = true;
        onAnimation.enabled = true;

    }


    public void MainMenu(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void close()
    {
        confirm.SetActive(true);
    }

    public void no()
    {
        confirm.SetActive(false);
    }
    public void yes()
    {
        Application.Quit();
    }
}
