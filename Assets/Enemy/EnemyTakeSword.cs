﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTakeSword : MonoBehaviour
{

    [SerializeField] protected Animator animator;
    [SerializeField] protected GameObject katanaOnBack, katanaOnHand;


    protected virtual void TakeKatana()
    {
        
            katanaOnBack.SetActive(false);
            katanaOnHand.SetActive(true);
        
    }
}
