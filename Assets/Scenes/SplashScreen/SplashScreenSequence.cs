﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreenSequence : MonoBehaviour
{

    public GameObject text, text1, text2;
    public FadeText[] fadeText;



    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine(SplashScreenText());



    }

    IEnumerator SplashScreenText()
    {
        text.SetActive(true);
        fadeText[0].FadeInText();
        yield return new WaitForSeconds(4f);
        text.SetActive(false);

        yield return new WaitForSeconds(1);
        
        text1.SetActive(true);
        fadeText[1].FadeInText();
        yield return new WaitForSeconds(4f);
        text1.SetActive(false);

        yield return new WaitForSeconds(1);

        text2.SetActive(true);
        fadeText[2].FadeInText();        
        yield return new WaitForSeconds(5f);
        text2.SetActive(false);

        yield return new WaitForSeconds(1);

        SceneManager.LoadScene("MainMenu");
    }
    
   

}
