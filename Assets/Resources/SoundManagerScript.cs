using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public AudioClip shootSound, walk,run,swordhack,swordhit,swordin,
    swordout,gravityin,gravityout,chidori,lights,deathenemy,replacementskill,shoothit,bossAttack,succestuto;
    public AudioSource audioSource;
    public Volume volume;
   
    // Start is called before the first frame update
    void Start()
    {
        shootSound = Resources.Load<AudioClip>("shuriken");
        walk = Resources.Load<AudioClip>("walk");
        run = Resources.Load<AudioClip>("run");
        swordhack=Resources.Load<AudioClip>("swordhack");
        swordhit=Resources.Load<AudioClip>("swordhit");
        swordin=Resources.Load<AudioClip>("swordin");
        swordout=Resources.Load<AudioClip>("swordout");
        gravityin=Resources.Load<AudioClip>("gravityin");
        gravityout=Resources.Load<AudioClip>("gravityout");
        lights=Resources.Load<AudioClip>("lightsoftr");
        deathenemy=Resources.Load<AudioClip>("death1");
        replacementskill=Resources.Load<AudioClip>("troncosfx");
        shoothit = Resources.Load<AudioClip>("shurikenhit2");
        chidori = Resources.Load<AudioClip>("Chidorisoft");
        bossAttack = Resources.Load<AudioClip>("BossAttack");
        succestuto= Resources.Load<AudioClip>("successound");





        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void pause(string clip){
        
    }
    public void PlaySound(string clip)
    {
        //audioSource.volume=1f;
        switch (clip)
        {
            
            //checked
            case "shoot":
            audioSource.PlayOneShot(shootSound);
                break;
            //checked
            case "run":
                audioSource.clip=run;
                if(!audioSource.isPlaying){
                    audioSource.Play();
                }else{
                     
                }
                break;
            //checked
            case "walk":
                audioSource.clip=walk;
                if(!audioSource.isPlaying){
                    audioSource.Play();
                }else{
                    
                }
                
                break;
            case "bossAttack":
                //audioSource.volume=0.1f;
                audioSource.clip = bossAttack;
                if (!audioSource.isPlaying)
                {
                    audioSource.Play();
                }
                else
                {

                }
                //audioSource.PlayOneShot(bossAttack);
                break;
            //checked
            case "attack":
                audioSource.PlayOneShot(swordhack,volume.volume/2);
                break;
                //checked
            case "hit":
                audioSource.PlayOneShot(swordhit,volume.volume/2);
                break;
            case "in":
                audioSource.PlayOneShot(swordin,volume.volume/2);
                break;
            case "out":
                audioSource.PlayOneShot(swordout,volume.volume/2);
                break;    
            case "Gin":
                audioSource.PlayOneShot(gravityin);
                break;  
            case "Gout":
                audioSource.PlayOneShot(gravityout);
                break;  
            case "light":
                audioSource.PlayOneShot(lights);
                break;  
            case "deathenemy":
                //audioSource.volume=0.1f;
                audioSource.PlayOneShot(deathenemy);
                break;  
            case "Sreplacement":
                audioSource.PlayOneShot(replacementskill);
                break;      
            case "shurikenhit":
                audioSource.PlayOneShot(shoothit);
                break; 
            case "chidori":
                audioSource.PlayOneShot(chidori);
                break;
            case "tuto":
                audioSource.PlayOneShot(succestuto,volume.volume/0.8f);
                break;


        }
    }
    
}
