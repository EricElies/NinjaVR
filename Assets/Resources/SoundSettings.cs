﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSettings : MonoBehaviour
{
    public Volume sfx;
    public Volume ambfx;

    public Volume mfx;
    
    bool mute =false;
    bool mute1 =false;
    bool mute2 =false;
    
    public void Ambient(){
       
        mute=!mute;
        if(mute==true){
            ambfx.volume=0;
        }else{
            ambfx.volume=0.7f; 
        }
       
    }
    public void SFX(){
        mute1=!mute1;
        if(mute1==true){
            sfx.volume=0;
        }else{
            sfx.volume=0.7f; 
        }
    }

    public void music(){
        mute2=!mute2;
        if(mute2==true){
            mfx.volume=0;
        }else{
            mfx.volume=0.5f; 
        }
    }
}
