﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroBoss : MonoBehaviour
{

    public PlayerController player;
    public bool firstTime, runningAnimation;
    public GameObject text,text1,text2,  fireParticlesCircle, bossBeforeEvolution, bossAfterEvolution;
    public Animator animator;
    public ParticleSystem particle;
    public PlayerOnAnimation playerOnAnimation;


    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerController>(); 
        firstTime = true;
        fireParticlesCircle.SetActive(false);
        bossAfterEvolution.SetActive(false);
        bossBeforeEvolution.SetActive(true);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && firstTime) 
        {
            firstTime = false;
            StartCoroutine(BossTalk1());
        }
    }


    IEnumerator BossTalk1() 
    {

        playerOnAnimation.runningAnimation = true;
        //player.enabled = false;
        //speechBuble.SetActive(true);
        text.SetActive(true);
        animator.SetTrigger("Start");
        yield return new WaitForSeconds(3);
        text.SetActive(false);
        yield return new WaitForSeconds(3);
        text1.SetActive(true);
        fireParticlesCircle.SetActive(true);
        yield return new WaitForSeconds(3);
        text1.SetActive(false);
        yield return new WaitForSeconds(3);
        text2.SetActive(true);
        yield return new WaitForSeconds(3);
        text2.SetActive(false);
        yield return new WaitForSeconds(3);
        //speechBuble.SetActive(false);
        animator.SetTrigger("Evolution");
        yield return new WaitForSeconds(3);
        bossAfterEvolution.SetActive(true);
        bossBeforeEvolution.SetActive(false);
        particle.gameObject.transform.localScale = new Vector3 (1,1,1);
        particle.startColor = Color.red;
        //player.enabled = true;
        playerOnAnimation.runningAnimation = false;

    }
}
