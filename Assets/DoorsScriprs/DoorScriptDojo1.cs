﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorScriptDojo1 : MonoBehaviour
{
    public string Scene;

    private void OnTriggerEnter(Collider co)
    {
        if (co.gameObject.CompareTag("Player"))
        {
            Cursor.lockState = CursorLockMode.None;
            Debug.Log("collid4e");
            SceneManager.LoadScene(Scene);
        }
    }
}
