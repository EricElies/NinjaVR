﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    public Animator anim;
    public float cooldown = 10f;
    public GameObject particle, particleAura;
    public Transform SpawnPoint;
    public int health = 100;
    public Transform Player;
    public Transform enemy;

    public Fade fade;
    //enemy SM methods

    /*[SerializeField] protected Transform realTimePlayerLocation;
    [SerializeField] protected float distanceToStartFight;
    protected Vector3 latestKnownPlayerLocation;
    protected NavMeshAgent agent;*/

    //manager methods
    public AudioSource auso;
    public SoundManagerScript sm;


    // Start is called before the first frame update
    void Start()
    {

        anim.GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        cooldown -= Time.deltaTime;
        if (cooldown <= 0)
        {

            anim.SetTrigger("Attack");
            attackBoss();
            cooldown = 10f;
        }

        if(health <= 0) { Death();  }

        

    }

    IEnumerator changeLevel()
    {
        yield return new WaitForSeconds(3f);
        fade.FadeOut();
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Comming Soon Scene");
    }

    /*protected virtual void startToAttack()
    {
        cooldown -= Time.deltaTime;
        if (cooldown <= 0)
        {
            //enemy.LookAt(Player);

            anim.SetTrigger("Attack");
            //particle.SetActive(true
            attackBoss();
            cooldown = 10f;
        }
    }*/

    IEnumerator attackBoss()
    {
        particle.SetActive(true);
        sm.PlaySound("bossAttack");
        yield return new WaitForSeconds(1.1f);
        particle.SetActive(false);

    }



    /*protected virtual void ApproachPlayer()
    {
        agent.SetDestination(realTimePlayerLocation.position);

        if (CheckDistance(realTimePlayerLocation) <= distanceToStartFight)
        {
            startToAttack();
        }
    }

    //Funciones para medir distancias
    protected virtual float CheckDistance(Transform target)
    {
        return Vector3.Distance(target.position, transform.position);
    }




    //Funciones relativas a la ubicación del jugador
    protected virtual void SetLatestPlayerLocation()
    {
        latestKnownPlayerLocation = realTimePlayerLocation.position;
    }*/

    protected virtual void Death()
    {
        StartCoroutine("deathsound");
        Debug.Log(gameObject.name + " ha muerto");
        anim.SetTrigger("IsDead");
        gameObject.tag = "EnemyDead";
        particleAura.SetActive(false);
        //sm.enabled=false;
        StartCoroutine(changeLevel());



    }

    IEnumerator deathsound()
    {
        if (sm.enabled == true)
        {
            sm.PlaySound("deathenemy");
            sm.enabled = false;
            yield return null;
        }
    }


    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (!this.CompareTag("EnemyDead")) { 
            if (collision.gameObject.CompareTag("WeaponPlayer"))
        {

            MeshRenderer go = collision.gameObject.GetComponent<MeshRenderer>();
            BoxCollider bc = go.GetComponent<BoxCollider>();
            GameObject gmash = go.gameObject.GetComponent<GameObject>();
            Destroy(gmash);
            bc.enabled = false;
            sm.PlaySound("shurikenhit");
            health -= 1;

        }

        //GameObject go=collision.gameObject.GetComponent<GameObject>();
        //go.GetComponent<Rigidbody>().constraints=RigidbodyConstraints.FreezePositionX;


        if (collision.gameObject.CompareTag("swordplayer"))
        {

            new WaitForSeconds(2.0f);
            sm.PlaySound("hit");

            health -= 1;
        }}
        

    }
}
