﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOnAnimation : MonoBehaviour
{
    PlayerController playerController;
    SlowTimeSkill skillSlow;
    GravitySkill gravitySkill;
    Shooting shooting;
    SworsAttack sworsAttack;
    public bool runningAnimation = false; 
    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<PlayerController>();
        skillSlow = GetComponent<SlowTimeSkill>();
        gravitySkill = GetComponent<GravitySkill>();
        shooting = GetComponent<Shooting>();
        sworsAttack = GetComponent<SworsAttack>();
        
    }

    // Update is called once per frame
    void Update()
    {

        activateAnimation();

    }

    public void activateAnimation() 
    {
        if (runningAnimation)
        {
            playerController.enabled = false;
            skillSlow.enabled = false;
            gravitySkill.enabled = false;
            shooting.enabled = false;
            sworsAttack.enabled = false;
        }

        else 
        {
            playerController.enabled = true;
            skillSlow.enabled = true;
            gravitySkill.enabled = true;
            shooting.enabled = true;
            sworsAttack.enabled = true;
        }
    }
}
